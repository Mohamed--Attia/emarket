//accordion
function accordion() {
    "use strict";
    var acc = document.getElementsByClassName("accordion"),
        i;

    for (i = 0; i < acc.length; i++) {
        acc[i].onclick = function () {
            this.classList.toggle("active");
            this.nextElementSibling.classList.toggle("show");
        }
    }
} // end accordion
$(document).ready(function () {

    //search
    $( "#inputSuccess2" ).keypress(function() {
        $(".autocomplete-overlay").css("display","block");
      });
      $(".autocomplete-overlay").click(function(){
        $(this).css("display","none");
      })
    //date picker
    // $("#startDate").datepicker()
    $('.clockpicker').clockpicker({
        placement: 'top',
        align: 'left',
        donetext: 'Done'
    });

    //address
    $('input[name="radio2"]').click(function () {
        if ($("#radio-6").is(':checked')) {
            $(".newAddress").slideDown(1000)
        } else {
            $(".newAddress").slideUp(1000)
        }
    });

    // product - profile - home
    $('.nav-tabs-dropdown').each(function (i, elm) {
        $(elm).text($(elm).next('ul').find('a.active li').text());
    });

    //plus && min
    var quantitiy = 0;
    $('.quantity-right-plus').click(function (e) {
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        var quantity = parseInt($('#quantity').val());
        // If is not undefined
        $('#quantity').val(quantity + 1);
        // Increment

    });

    $('.quantity-left-minus').click(function (e) {
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        var quantity = parseInt($('#quantity').val());

        // If is not undefined
        // Increment
        if (quantity > 0) {
            $('#quantity').val(quantity - 1);
        }
    });

    //add on cart
    $(".ddd").on("click", function () {

        var $button = $(this);
        var oldValue = $button.closest('.sp-quantity').find("input.quntity-input").val();

        if ($button.text() == "+") {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            // Don't allow decrementing below zero
            if (oldValue > 0) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0;
            }
        }

        $button.closest('.sp-quantity').find("input.quntity-input").val(newVal);

    });

    //hide peoduct
    $(".product .fa-trash").click(function () {
        $(this).closest('.product').remove()
    })
});

$(document).ready(function () {
    $("#owl-demo").owlCarousel({
        autoPlay: 3000, //Set AutoPlay to 3 seconds
        items: 4,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [979, 3]
    });
    $("#owl-demo1").owlCarousel({
        autoPlay: 3000, //Set AutoPlay to 3 seconds
        items: 6,
        itemsDesktop: [1199, 4],
        itemsDesktopSmall: [979, 4]
    });

    // owl-carousel alignment
    if ($("#owl-demo .item").length == 3) {
        $(".owl-carousel").css("right", "-333px")
    }
});